# HelmRepoly

Create Helm repositories from miscallenious Git repositories.

## Usage

Add your git repos to the `example.yaml` and if those have named repositories as dependencies (For example `@bitnami`)

Example entry:
```yaml
charts:
  - name: NOT-USED
    git: https://open.greenhost.net/openappstack/wordpress-helm
    helmrepodeps:
      bitnami: https://charts.bitnami.com/bitnami
```

