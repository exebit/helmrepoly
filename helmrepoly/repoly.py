#!/bin/python3
import yaml
import tempfile
import shutil
import subprocess
from git import Repo
from pathlib import Path
import os


def run():
    with open("example.yaml") as conf_file:
        conf = yaml.load(conf_file, Loader=yaml.Loader)

    # packed_repo_dir = Path(tempfile.mkdtemp())
    packed_repo_dir = Path.cwd() / "public"
    os.mkdir(packed_repo_dir)

    for repo in conf["charts"]:
        # Clone chart from git
        tdir = Path(tempfile.mkdtemp())
        Repo.clone_from(repo["git"], tdir)

        # Handle named repository dependencies
        for name, url in repo.get("helmrepodeps", {}).items():
            subprocess.run(["helm", "repo", "add", name, url], check=True)
        subprocess.run(["helm", "dependency", "update"], cwd=tdir, check=True)

        # Package the chart
        chart_path = tdir / repo.get("path", ".")
        subprocess.run(["helm", "package", chart_path], cwd=packed_repo_dir, check=True)

        # Delete named repos
        for name, url in repo.get("helmrepodeps", {}).items():
            subprocess.run(["helm", "repo", "remove", name], check=True)
        shutil.rmtree(tdir)

    # Create index.yaml
    helm_command = ["helm", "repo", "index", packed_repo_dir]
    if "HELM_BASE_URL" in os.environ:
        helm_command += ["--url", os.environ["HELM_BASE_URL"]]
    elif "base_url" in conf:
        helm_command += ["--url", conf["base_url"]]

    subprocess.run(helm_command, cwd=packed_repo_dir, check=True)


if __name__ == "__main__":
    run()
